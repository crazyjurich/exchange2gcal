import logging
import os
import urllib2
import xml.etree.ElementTree as ET

import nltk
from ntlm import HTTPNtlmAuthHandler
import rfc3339

import config
import calendarEvent
import exchangeRecurrence

log = logging.getLogger(__name__)

class ExchangeCalendar:
    def __init__(self):
        passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, config.EXCHANGE_URL, config.EXCHANGE_USER, config.EXCHANGE_PASSWORD)
        handler = HTTPNtlmAuthHandler.HTTPNtlmAuthHandler(passman)
        # TODO: do I need these?
        #https = urllib2.HTTPSHandler()
        #opener = urllib2.build_opener(https, handler)
        opener = urllib2.build_opener(handler)
        urllib2.install_opener(opener)

        self._NS = {"soap" : "http://schemas.xmlsoap.org/soap/envelope/",
                    "m"    : "http://schemas.microsoft.com/exchange/services/2006/messages",
                    "t"    : "http://schemas.microsoft.com/exchange/services/2006/types"}
        if config.DEBUG_DUMP_MESSAGES:
            self._dumpfileCounter = 0
            if not os.path.isdir("debug"):
                os.makedirs("debug")

    def _DebugDumpFile(self, req_id, params, data):
        """Write requests and responses to files in debug directory"""
        if config.DEBUG_DUMP_MESSAGES:
            #serialized_params = "&".join(["%s=%s"%i for i in params.items()]) # this produces invalid filenames, disable
            dumpfilename = "%s_%s" % (self._dumpfileCounter, req_id) 
            open(os.path.join("debug", dumpfilename), "w").write(data)
            self._dumpfileCounter += 1

    def _DateCompose(self, dt):
        return rfc3339.datetimetostr(dt)

    def _DateParse(self, datestr):
        return rfc3339.parse_datetime(datestr)

    def _HTML2TXT(self, html):
        return nltk.clean_html(html)

    def _SendRequest(self, req_id, params=None):
        if params is None:
            params = {}
        for key in params:
            if params[key] is None:
                params[key] = '' # prevent <SyncState>None</SyncState>, it breaks everything
        reqdata = REQUESTS[req_id] % params
        self._DebugDumpFile(req_id, params, reqdata)
        headers = { 'Content-Type' : 'text/xml; charset=utf-8',
                    'SOAPAction': "http://schemas.microsoft.com/exchange/services/2006/messages/%s" % req_id }
        req = urllib2.Request(config.EXCHANGE_URL, reqdata, headers)
        resp = urllib2.urlopen(req)
        if resp.getcode() != 200:
            print resp.read()
            raise Exception("Request failed: HTTP %s" % resp.getcode())
        respdata = resp.read()
        self._DebugDumpFile(req_id, params, respdata)
        return respdata

    def GetEvent(self, eid, isOccurrence=False):
        xmldata = self._SendRequest("GetItem", {"ItemId":eid})
        rootNode = ET.fromstring(xmldata)
        ft = lambda n, t: n.find(t, namespaces=self._NS)
        respNode = ft(rootNode, "soap:Body")[0][0][0]
        itemNode = ft(respNode, "m:Items/t:CalendarItem")
        recurrenceNode  = ft(itemNode, "t:Recurrence")
        attendeesNode   = ft(itemNode, "t:RequiredAttendees")
        modifiedOccNode = ft(itemNode, "t:ModifiedOccurrences")
        deletedOccNode  = ft(itemNode, "t:DeletedOccurrences")
        remindBeforeNode= ft(itemNode, "t:ReminderMinutesBeforeStart")
        bodyNode        = ft(itemNode, "t:Body");
        attendees = []
        exdates = []
        modOccs = []
        if attendeesNode is not None:
            for attendeeNode in attendeesNode:
                name  = ft(attendeeNode, "t:Mailbox/t:Name").text
                email = ft(attendeeNode, "t:Mailbox/t:EmailAddress").text
                if '@' not in email:
                    # sometimes Exchange sends junk in this field, something from LDAP
                    # In my case it was an ex employee so dont care
                    email = None
                attendees.append(calendarEvent.Attendee(name, email))
        if deletedOccNode is not None:
            for delOccNode in deletedOccNode:
                start = ft(delOccNode, "t:Start").text
                exdates.append(self._DateParse(start))
        if modifiedOccNode is not None:
            for modOccNode in modifiedOccNode:
                eid = ft(modOccNode, "t:ItemId").attrib["Id"]
                modOccs.append(self.GetEvent(eid, isOccurrence=True))
        event = calendarEvent.CalendarEvent(
            eid           = eid,
            summary       = ft(itemNode, "t:Subject").text,
            description   = "" if not bodyNode is None else self._HTML2TXT(bodyNode.text),
            start         = self._DateParse(ft(itemNode, "t:Start").text),
            end           = self._DateParse(ft(itemNode, "t:End").text),
            originalStart = None if not isOccurrence else self._DateParse(ft(itemNode, "t:OriginalStart").text),
            location      = ft(itemNode, "t:Location").text,
            rrule         = None if recurrenceNode is None else exchangeRecurrence.RecurrenceEws2rfc2445().Convert(recurrenceNode),
            modifiedOccurrences = modOccs,
            exdates       = exdates,
            organizer     = ft(itemNode, "t:Organizer/t:Mailbox/t:Name").text,
            attendees     = attendees,
            remindBefore  = None if remindBeforeNode is None else int(remindBeforeNode.text),
        )
        return event

    def GetChanges(self, syncstate, startdate=None):
        ft = lambda n, t: n.find(t, namespaces=self._NS)
        xmldata = self._SendRequest("SyncFolderItems", {"SyncState":syncstate})
        rootNode = ET.fromstring(xmldata)
        respNode = rootNode.find("soap:Body", namespaces=self._NS)[0][0][0]
        newSyncState = respNode.find("m:SyncState", namespaces=self._NS).text

        if respNode.attrib["ResponseClass"] == "Error":
            errmsg = ft(respNode, "m:MessageText").text
            raise Exception("EWS 'SyncFolderItems' call failed: %r" % errmsg)
        
        changesNode = ft(respNode, "m:Changes")
        events = []
        for changeNode in changesNode:
            ctype = changeNode.tag.split("}")[-1].lower()
            if ctype not in ["create", "update", "delete"]:
                raise Exception("Unknown tag: %s" % changeNode)
            itemNode = changeNode[0]
            if ctype == "delete":
                eid = itemNode.attrib["Id"]
                e = calendarEvent.CalendarEvent(eid, None)
                e.action = ctype
                events.append(e)
                continue
            eid = ft(itemNode, "t:ItemId").attrib["Id"]
            end = self._DateParse(ft(itemNode, "t:End").text)
            itype = ft(itemNode, "t:CalendarItemType").text
            if startdate is None or itype == "RecurringMaster" or startdate <= end.date():
                try:
                    e = self.GetEvent(eid)
                except Exception as e:
                    log.exception("Unhandled exception while fetching Event from Exchange")
                    raise Exception("Unhandled exception while fetching Event %r from Exchange: %s" % (eid, e))
                e.action = ctype
                events.append(e)
        return (newSyncState, events)


    def _SendRequest2(self, req_id, params=None):
        # TODO: integrate with _SendRequest()
        ft = lambda n, t: n.find(t, namespaces=self._NS)
        xmldata = self._SendRequest(req_id, params)
        rootNode = ET.fromstring(xmldata)
        respNode = rootNode.find("soap:Body", namespaces=self._NS)[0][0][0]
        if respNode.attrib["ResponseClass"] == "Error":
            errmsg = ft(respNode, "m:MessageText").text
            raise Exception("EWS 'FindItem' call failed: %r" % errmsg)
        return respNode

    def AddEvent(self, event):
        # TODO: more fields
        d = dict(event.__dict__)
        d["start"] = self._DateCompose(d["start"])
        d["end"]   = self._DateCompose(d["end"])
        self._SendRequest2("CreateItem", d)

    def GetAllEventIds(self):
        ft = lambda n, t: n.find(t, namespaces=self._NS)
        respNode = self._SendRequest2("FindItem")
        items = ft(respNode, "m:RootFolder/t:Items")
        ids = []
        for item in items:
            ids.append(item[0].attrib["Id"])
        return ids

    def GetAllEvents(self):
        return [self.GetEvent(eid) for eid in self.GetAllEventIds()]

    def DeleteEvents(self, eids):
        if not eids:
            return 0
        items = '\n'.join(['<t:ItemId Id="%s"/>' % eid for eid in eids])
        self._SendRequest2("DeleteItem", {"items":items})
        return len(eids)

    def Clear(self):
        return self.DeleteEvents(self.GetAllEventIds())

  # <xs:simpleType name="CalendarItemTypeType">
  #   <xs:restriction base="xs:string">
  #     <xs:enumeration value="Single" />
  #     <xs:enumeration value="Occurrence" />
  #     <xs:enumeration value="Exception" />
  #     <xs:enumeration value="RecurringMaster" />
  #   </xs:restriction>
  # </xs:simpleType>

  # <xs:complexType name="SyncFolderItemsChangesType">
  #   <xs:sequence>
  #     <xs:choice maxOccurs="unbounded" minOccurs="0">
  #       <xs:element name="Create" type="t:SyncFolderItemsCreateOrUpdateType"/>
  #       <xs:element name="Update" type="t:SyncFolderItemsCreateOrUpdateType"/>
  #       <xs:element name="Delete" type="t:SyncFolderItemsDeleteType"/>
  #       <xs:element name="ReadFlagChange" type="t:SyncFolderItemsReadFlagType"/>
  #     </xs:choice>
  #   </xs:sequence>
  # </xs:complexType>

REQUESTS = {
"GetItem":"""\
<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:m="http://schemas.microsoft.com/exchange/services/2006/messages"
            xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types">
   <s:Body>
      <m:GetItem>
         <m:ItemShape>
            <t:BaseShape>AllProperties</t:BaseShape>
         </m:ItemShape>
         <m:ItemIds>
            <t:ItemId Id="%(ItemId)s"/>
         </m:ItemIds>
      </m:GetItem>
   </s:Body>
</s:Envelope>""",
# TODO: getting items in piles probably will be faster
# TODO: use one common XML header for all hardcoded requests
"SyncFolderItems":"""\
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
               xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types">
  <soap:Body>
    <SyncFolderItems xmlns="http://schemas.microsoft.com/exchange/services/2006/messages">
      <ItemShape>
        <t:BaseShape>IdOnly</t:BaseShape>
        <t:AdditionalProperties>
          <t:FieldURI FieldURI="item:Subject"/>
          <t:FieldURI FieldURI="calendar:Start"/>
          <t:FieldURI FieldURI="calendar:End"/>
          <t:FieldURI FieldURI="calendar:CalendarItemType"/>
        </t:AdditionalProperties>
      </ItemShape>
      <SyncFolderId>
        <t:DistinguishedFolderId Id="calendar"/>
      </SyncFolderId>
      <SyncState>%(SyncState)s</SyncState>
      <MaxChangesReturned>512</MaxChangesReturned>
    </SyncFolderItems>
  </soap:Body>
</soap:Envelope>""",
# TODO: remove start and subject

"DeleteItem":"""\
<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:m="http://schemas.microsoft.com/exchange/services/2006/messages"
            xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types">
  <s:Body>
    <m:DeleteItem DeleteType="HardDelete" SendMeetingCancellations="SendToNone">
      <m:ItemIds>
        %(items)s
      </m:ItemIds>
    </m:DeleteItem>
  </s:Body>
</s:Envelope>""",

"CreateItem":"""\
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
               xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types">
  <soap:Body>
    <CreateItem xmlns="http://schemas.microsoft.com/exchange/services/2006/messages"
                xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types" 
                SendMeetingInvitations="SendToNone" >
      <SavedItemFolderId>
        <t:DistinguishedFolderId Id="calendar"/>
      </SavedItemFolderId>
      <Items>
        <t:CalendarItem xmlns="http://schemas.microsoft.com/exchange/services/2006/types">
          <Subject>%(summary)s</Subject>
          <Body BodyType="Text">%(description)s</Body>
          <ReminderIsSet>true</ReminderIsSet>
          <ReminderMinutesBeforeStart>60</ReminderMinutesBeforeStart>
          <Start>%(start)s</Start>
          <End>%(end)s</End>
          <IsAllDayEvent>false</IsAllDayEvent>
          <LegacyFreeBusyStatus>Busy</LegacyFreeBusyStatus>
          <Location>Conference Room 721</Location>
          <RequiredAttendees>
            <Attendee>
              <Mailbox>
                <EmailAddress>User1@example.com</EmailAddress>
              </Mailbox>
            </Attendee>
            <Attendee>
              <Mailbox>
                <EmailAddress>User2@example.com</EmailAddress>
              </Mailbox>
            </Attendee>
          </RequiredAttendees>
        </t:CalendarItem>
      </Items>
    </CreateItem>
  </soap:Body>
</soap:Envelope>""",

"FindItem":"""\
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
               xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types">
  <soap:Body>
    <FindItem xmlns="http://schemas.microsoft.com/exchange/services/2006/messages"
               xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types"
              Traversal="Shallow">
      <ItemShape>
        <t:BaseShape>IdOnly</t:BaseShape>
      </ItemShape>
      <ParentFolderIds>
        <t:DistinguishedFolderId Id="calendar"/>
      </ParentFolderIds>
    </FindItem>
  </soap:Body>
</soap:Envelope>"""

}
