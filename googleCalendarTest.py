import datetime

import calendarEvent
import googleCalendar

def check(label, func):
    if func():
        print "%s ... OK" % label
    else:
        print "%s ... FAILED" % label

gsvc = googleCalendar.GoogleService()

gcl = gsvc.GetCalendarList()

print "User has %s calendars:" % len(gcl)

for cal in gcl:
    print "  %s" % cal

print "Primary calendar is %s" % gcl.GetPrimaryCalendar()

print "Creating test calendar"
cal = gcl.CreateCalendar("test")

check("New calendar has no events", lambda: len(cal.GetEvents()) == 0)

event = calendarEvent.CalendarEvent(
    eid         = None,
    summary     = "test event",
    description = "test event description",
    start       = datetime.datetime.now(),
    end         = datetime.datetime.now() + datetime.timedelta(hours=1),
    location    = "event location",
    #rrule       = "FREQ=WEEKLY;INTERVAL=1;BYDAY=TU,FR"
    )

def cbk(request_id, response, exception):
    if exception:
        print "callback says FAILED"
    else:
        print "callback says OK"

print "Creating test event"
cal.AddEvent(event, cbk)
cal.Flush()

check("Now calendar has one event", lambda: len(cal.GetEvents()) == 1)

print "Deleting test calendar"
cal.Delete()

print "DONE"