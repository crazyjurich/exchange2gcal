"""This module converts EWS GetItem service response node <t:Recurrence> to rfc2445 format"""
# http://www.ietf.org/rfc/rfc2445

import datetime
import re

class RecurrenceEws2rfc2445:

    def _ConvDay(self, daystr):
        convdict = {
            "Monday"    : "MO",
            "Tuesday"   : "TU",
            "Wednesday" : "WE",
            "Thursday"  : "TH",
            "Friday"    : "FR",
            "Saturday"  : "SA",
            "Sunday"    : "SU",
        }
        return convdict[daystr]

    def _ConvDays(self, daystr):
        return ",".join([self._ConvDay(d) for d in daystr.split(' ')])

    def _ConvDate(self, datestr, isEndDate=False):
        if re.match(r"\d\d\d\d-\d\d-\d\dZ", datestr):
            rdate = datetime.datetime.strptime(datestr[:-1], "%Y-%m-%d")
        elif re.match(r"\d\d\d\d-\d\d-\d\d\+\d\d:\d\d", datestr):
            rdate = datetime.datetime.strptime(datestr[:-6], "%Y-%m-%d")
        else:
            raise Exception("Wrong date string %r" % datestr)
        
        if isEndDate:
            rdate += datetime.timedelta(days=1)
        # 19971224T000000Z
        return rdate.strftime("%Y%m%dT000000Z")

    def _ConvWeekIdx(self, weekIdxStr):
        convdict = {
            "First"  : "1",
            "Second" : "2",
            "Third"  : "3",
            "Fourth" : "4",
            "Last"   : "-1",
        }
        return convdict[weekIdxStr]

    def _ConvPattern(self, pnode):
        patternType = pnode.tag.split('}')[1]
        interval = lambda: int(pnode[0].text)
        dows     = lambda: self._ConvDays(pnode[1].text)
        weekidx  = lambda: self._ConvWeekIdx(pnode[2].text)
        if patternType == "DailyRecurrence":
            res = "FREQ=DAILY;INTERVAL=%s" % interval()
        elif patternType == "WeeklyRecurrence":
            res = "FREQ=WEEKLY;INTERVAL=%s;BYDAY=%s" % (interval(), dows())
        elif patternType == "RelativeMonthlyRecurrence":
            res = "FREQ=MONTHLY;INTERVAL=%s;BYDAY=%s%s" % (interval(), weekidx(), dows())
        else:
            raise Exception("Unsupported recurrence pattern %r" % patternType)
        return res

    def _ConvRange(self, rnode):
        rangeType   = rnode.tag.split('}')[1]
        res = ""
        if rangeType in ["NoEndRecurrence", "EndDateRecurrence", "NumberedRecurrence"]:
            #startDate = rnode[0].text  # Not needed ?
            if rangeType == "EndDateRecurrence":
                endDate = self._ConvDate(rnode[1].text, isEndDate=True)
                res = ";UNTIL=%s" % endDate
            elif rangeType == "NumberedRecurrence":
                num = int(rnode[1].text)
                res = ";COUNT=%s" % num
        else:
            raise Exception("Unsupported recurrence range %r" % rangeType)
        return res

    def Convert(self, rnode):
        patternNode = rnode[0]
        rangeNode = rnode[1]
        return self._ConvPattern(patternNode) + self._ConvRange(rangeNode)



  # <xs:element name="Recurrence" type="t:RecurrenceType" minOccurs="0" />

  # <xs:complexType name="RecurrenceType">
  #   <xs:sequence>
  #     <xs:group ref="t:RecurrencePatternTypes" />
  #     <xs:group ref="t:RecurrenceRangeTypes" />
  #   </xs:sequence>
  # </xs:complexType>


  # <xs:group name="RecurrencePatternTypes">
  #   <xs:sequence>
  #     <xs:choice>
  #       <xs:element name="RelativeYearlyRecurrence" type="t:RelativeYearlyRecurrencePatternType" />
  #       <xs:element name="AbsoluteYearlyRecurrence" type="t:AbsoluteYearlyRecurrencePatternType" />
  #       <xs:element name="RelativeMonthlyRecurrence" type="t:RelativeMonthlyRecurrencePatternType" />
  #       <xs:element name="AbsoluteMonthlyRecurrence" type="t:AbsoluteMonthlyRecurrencePatternType" />
  #       <xs:element name="WeeklyRecurrence" type="t:WeeklyRecurrencePatternType" />
  #       <xs:element name="DailyRecurrence" type="t:DailyRecurrencePatternType" />
  #     </xs:choice>
  #   </xs:sequence>
  # </xs:group>

  # <xs:group name="RecurrenceRangeTypes">
  #   <xs:sequence>
  #     <xs:choice>
  #       <xs:element name="NoEndRecurrence" type="t:NoEndRecurrenceRangeType" />        -> StartDate
  #       <xs:element name="EndDateRecurrence" type="t:EndDateRecurrenceRangeType" />    -> StartDate, EndDate
  #       <xs:element name="NumberedRecurrence" type="t:NumberedRecurrenceRangeType" />  -> StartDate, NumberOfOccurrences
  #     </xs:choice>
  #   </xs:sequence>
  # </xs:group>


# <t:WeeklyRecurrence>
#     <t:Interval>1</t:Interval>
#     <t:DaysOfWeek>Wednesday</t:DaysOfWeek>
# </t:WeeklyRecurrence>

# <t:DaysOfWeek>Tuesday Friday</t:DaysOfWeek>

  #   <xs:simpleType name="DayOfWeekType">
  #   <xs:restriction base="xs:string">
  #     <xs:enumeration value="Sunday" />
  #     <xs:enumeration value="Monday" />
  #     <xs:enumeration value="Tuesday" />
  #     <xs:enumeration value="Wednesday" />
  #     <xs:enumeration value="Thursday" />
  #     <xs:enumeration value="Friday" />
  #     <xs:enumeration value="Saturday" />
  #     <xs:enumeration value="Day" />
  #     <xs:enumeration value="Weekday" />
  #     <xs:enumeration value="WeekendDay" />
  #   </xs:restriction>
  # </xs:simpleType>