import datetime

import calendarEvent
import config
from exchangeCalendar import ExchangeCalendar

def ews_test():
    ecal = ExchangeCalendar()

    event = calendarEvent.CalendarEvent(
        eid         = None,
        summary     = "test event",
        description = "test event description",
        start       = datetime.datetime.now(),
        end         = datetime.datetime.now() + datetime.timedelta(hours=1),
        location    = "event location",
    )

    ecal.AddEvent(event)
    print "Added event"

    (newSyncState, changes) = ecal.GetChanges(None, config.SYNC_PAST_LIMIT)
    print "Got %s changes" % len(changes)

    events = ecal.GetAllEvents()
    print "Got %s events" % len(events)

    n = ecal.Clear()
    print "Calendar cleared (%s events)" % n

ews_test()