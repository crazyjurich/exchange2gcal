import argparse
from collections import Counter
import logging
import pickle
import pprint
import sys

from exchangeCalendar import ExchangeCalendar
import googleCalendar
import config

logging.basicConfig(filename='sync.log',
                    level=config.LOGLEVEL,
                    format="%(asctime)s|%(levelname)-7s|%(filename)-16s|%(lineno)-4s|%(message)s")
log = logging.getLogger(__name__)

class SyncException(Exception):
    pass

class Synchronizer:

    def __init__(self, stdout=False):
        self._store = None
        self._stdout = stdout
        self.LoadStore()

    def LoadStore(self):
        try:
            self._store = pickle.load(open("store.dat", "rb"))
            log.debug("Loaded store, %s EIDS, calendarId=%s, syncState=%s" %
                (len(self._store["EIDS"]), self._store["calendarId"], self._store["syncState"]))
        except IOError:
            log.warning("Performing initial setup")
            self._store = {"syncState" : None,
                           "calendarId" : None,
                           "EIDS" : {}}  # exchange eid : google eid

    def SaveStore(self):
        log.debug("Saving store, %s EIDS, calendarId=%s, syncState=%s" %
            (len(self._store["EIDS"]), self._store["calendarId"], self._store["syncState"]))
        pickle.dump(self._store, open("store.dat", "wb"))

    def ResetSyncState(self):
        log.debug("Performing sync state reset")
        self._store["syncState"] = None
        self._store["EIDS"] = {}
        self.SaveStore()

    def PrintProgress(self, msg):
        log.info(msg)
        if self._stdout:
            print(msg)

    def AlterEvent(self, event):
        if event.remindBefore is not None and not config.SYNC_REMINDERS:
            event.remindBefore = None

    def Sync(self, resync=False):
        self.PrintProgress("Initializing Google calendar...")
        gsvc = googleCalendar.GoogleService()
        gcallist = gsvc.GetCalendarList()
        if self._store["calendarId"]:
            gcal = gcallist.GetCalendar(id=self._store["calendarId"])
            if resync:
                self.ResetSyncState()
                gcal.Clear()
        else:
            if gcallist.Exists(config.GOOGLE_CALENDAR_NAME):
                raise SyncException("Google calendar %r already exists, terminating" % config.GOOGLE_CALENDAR_NAME)
            else:
                gcal = gcallist.CreateCalendar(config.GOOGLE_CALENDAR_NAME)
                self._store["calendarId"] = gcal.GetId()
                log.info("Created Google calendar %r, id=%r" % (config.GOOGLE_CALENDAR_NAME, gcal.GetId()))

        self.PrintProgress("Initializing Exchange calendar...")
        ecal = ExchangeCalendar()

        # TODO: getChanges must be able to fetch all changes, event if it needs many requests
        self.PrintProgress("Loading data from Exchange...")
        (newSyncstate, changes) = ecal.GetChanges(self._store["syncState"], config.SYNC_PAST_LIMIT)
        self.PrintProgress("Got %s changes from Exchange" % len(changes))

        counters = Counter()

        def callback(request_id, response, exception, **kwargs):
            #log.debug("Google api debug. Request =\n%s" % pprint.pformat(kwargs.get("request_body")))
            #log.debug("Google api debug. Response=\n%s" % pprint.pformat(response))
            if exception:
                # Probably we cannot fail here, some data is stored in google...
                log.exception("Exception in Google batch processing EID=%r request=%s" %
                    (kwargs.get("exchange_eid"), pprint.pformat(kwargs.get("request_body")))
                )
                counters["errors"] += 1
            else:
                if kwargs.get("exchange_eid"):
                    self._store["EIDS"][kwargs["exchange_eid"]] = response["id"]
                if "counter_type" in kwargs:
                    counters[kwargs["counter_type"]] += 1

        def warn(msg):
            log.warning(msg)
            counters["warnings"] += 1

        callbackParams = lambda exchange_eid, counter_type: locals()

        for i, e in enumerate(changes):
            log.debug("Processing change %s (action=%r, summary=%r, id=%r)" %
                (i+1, e.action, e.summary, e.eid))
            self.AlterEvent(e)
            if e.action == "create":
                if e.eid in self._store["EIDS"]:
                    warn("Ignored attemt to recreate existing event")
                else:
                    gcal.AddEvent(e, callback, callbackParams(e.eid, "created"))
            elif e.action == "update":
                if e.eid in self._store["EIDS"]:
                    eventId = self._store["EIDS"][e.eid]
                    gcal.UpdateEvent(e, eventId, callback, callbackParams(e.eid, "updated"))
                else:
                    warn("Asked to update not existing event, creating")
                    gcal.AddEvent(e, callback, callbackParams(e.eid, "created"))
            elif e.action == "delete":
                if e.eid in self._store["EIDS"]:
                    eventId = self._store["EIDS"][e.eid]
                    gcal.DeleteEvent(eventId, callback, callbackParams(None, "deleted"))
                else:
                    warn("Ignored attempt to delete not existing event")
            else:
                raise Exception("Undefined event action %r" % e.action)
        gcal.Flush() # callbacks get called on Flush

        log.debug("Starting modified occurrence sync")
        for i, e in enumerate(changes):
            for j, modOcc in enumerate(e.modifiedOccurrences):
                log.debug("Processing modified occurrence %s.%s (summary=%r, id=%r)" %
                    (i+1, j+1, modOcc.summary, modOcc.eid))
                self.AlterEvent(modOcc)
                eventId = self._store["EIDS"][e.eid]
                gcal.ModifyOccurrence(eventId, modOcc, callback, callbackParams(None, "occMofified"))
        gcal.Flush() 

        self._store["syncState"] = newSyncstate
        self.SaveStore()
        msg = """\
Created:  %(created)s
Updated:  %(updated)s
Modified occurrences: %(occMofified)s
Deleted:  %(deleted)s
Warnings: %(warnings)s
Errors:   %(errors)s""" % counters
        self.PrintProgress(msg)
        return counters

def main():
    log.info("Started")
    parser = argparse.ArgumentParser(
        description='Tool to synchronize MS Exchange events to Google Calendar')
    parser.add_argument('-r', '--resync', action='store_true',
        help='Perform resynchronization. Warning: all events from google calendar are deleted')
    args = parser.parse_args()
    try:
        sync = Synchronizer(stdout=True)
        sync.Sync(args.resync)
    except SyncException as e:
        print(e)
    except:
        log.exception("Unhandled exception")
        print("Unhandled exception, terminating, see log file for details")
        sync.SaveStore()
        sys.exit(1)
    sys.exit(0)

if __name__ == "__main__":
    main()