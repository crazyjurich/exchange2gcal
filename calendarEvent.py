
class Attendee:
    def __init__(self, name, email=None):
        if not name and not email:
            raise Exception("Both name and email cannot be undefined")
        self.name = name
        self.email = email

    def GetEmail(self, create_dummy_if_empty=False):
        if create_dummy_if_empty:
            if self.email:
                return self.email
            else:
                return self.name.replace(' ', '.').replace('@', '.') + "@undefined.dummy"
        else:
            return self.email

class CalendarEvent:
    def __init__(self, eid, summary, start=None, end=None, originalStart=None, description=None,
        location=None, rrule=None, exdates=None, modifiedOccurrences=None, timestamp=None, organizer=None,
        attendees=None, remindBefore=None):

        initlist = lambda p: [] if p is None else p
        self.eid = eid
        self.summary = summary
        self.start = start
        self.end = end
        self.originalStart = originalStart
        self.description = description
        self.location = location
        self.rrule = rrule
        self.exdates = initlist(exdates)
        self.modifiedOccurrences = initlist(modifiedOccurrences)
        self.timestamp = timestamp
        self.organizer = organizer
        self.attendees = initlist(attendees)
        self.remindBefore = remindBefore
        self.action = "create"

    def __eq__(self, other):
        return (self.summary == other.summary and
                self.start == other.start and
                self.end == other.end and
                self.description == other.description and
                self.location == other.location)

    def Print(self):
        print("summary      %s" % self.summary)
        print("start        %s" % self.start)
        print("end          %s" % self.end)
        #print("description  %s" % self.description)
        print("location     %s" % self.location)
        print("rrule        %s" % self.rrule)
        print("timestamp    %s" % self.timestamp)