import datetime

import calendarEvent
import sync

def test(ecal, gcal):
    event = calendarEvent.CalendarEvent(
        eid         = None,
        summary     = "test event",
        description = "test event description",
        start       = datetime.datetime.now(),
        end         = datetime.datetime.now() + datetime.timedelta(hours=1),
        location    = "event location",
    )

    ecal.AddEvent(event)

    sync.Synchronizer().Sync()

    return True