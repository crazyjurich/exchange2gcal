import os
import sys

import config
import exchangeCalendar
import googleCalendar
import sync

class Test:
    def __init__(self):
        self.case_path = "test/cases"
        self.ecal = exchangeCalendar.ExchangeCalendar()

        gsvc = googleCalendar.GoogleService()
        gcl = gsvc.GetCalendarList()
        self.gcal = gcl.GetCalendar(config.GOOGLE_CALENDAR_NAME)

    def compare_state(self):
        eevents = self.ecal.GetAllEvents()
        gevents = self.gcal.GetEvents()

        if len(eevents) == len(gevents):
            return True
        else:
            return False
            #print "Fail (G:%s E:%s)" % (len(gevents), len(eevents))

        # TODO: real compare!

    def reset(self):
        self.ecal.Clear()
        self.gcal.Clear()
        sync.Synchronizer().ResetSyncState()

    def run_one(self, case_file):
        self.reset()
        path, filename = os.path.split(case_file)
        if path not in sys.path:
            sys.path.append(path)
        modulename = filename.split('.')[0]
        m = __import__(modulename)
        return m.test(self.ecal, self.gcal) and self.compare_state()

    def run_all(self):
        succeded = failed = 0
        for filename in os.listdir(self.case_path):
            if not filename.endswith('.py'):
                continue
            print "%s ..." % filename,
            sys.stdout.flush()
            result = self.run_one(os.path.join(self.case_path, filename))
            if result:
                succeded += 1
                print "OK"
            else:
                failed += 1
                print "FAILED"
        print "\n%s Succeeded %s Failed" % (succeded, failed)
        return failed == 0

test = Test()
print "Starting tests"
test.run_all()