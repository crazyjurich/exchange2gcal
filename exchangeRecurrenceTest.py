
import xml.etree.ElementTree as ET

import exchangeRecurrence

CASES = [
    ("""\
        <t:WeeklyRecurrence>
            <t:Interval>1</t:Interval>
            <t:DaysOfWeek>Wednesday</t:DaysOfWeek>
        </t:WeeklyRecurrence>
        <t:NoEndRecurrence>
            <t:StartDate>2012-01-11Z</t:StartDate>
        </t:NoEndRecurrence>""",
        "FREQ=WEEKLY;INTERVAL=1;BYDAY=WE"),
    ("""\
        <t:WeeklyRecurrence>
            <t:Interval>1</t:Interval>
            <t:DaysOfWeek>Tuesday Friday</t:DaysOfWeek>
        </t:WeeklyRecurrence>
        <t:NoEndRecurrence>
            <t:StartDate>2013-12-24Z</t:StartDate>
        </t:NoEndRecurrence>""",
        "FREQ=WEEKLY;INTERVAL=1;BYDAY=TU,FR"),
    ("""\
        <t:WeeklyRecurrence>
            <t:Interval>2</t:Interval>
            <t:DaysOfWeek>Tuesday Friday</t:DaysOfWeek>
        </t:WeeklyRecurrence>
        <t:NumberedRecurrence>
            <t:StartDate>2013-12-24Z</t:StartDate>
            <t:NumberOfOccurrences>10</t:NumberOfOccurrences>
        </t:NumberedRecurrence>""",
        "FREQ=WEEKLY;INTERVAL=2;BYDAY=TU,FR;COUNT=10"),
    ("""\
        <t:DailyRecurrence>
            <t:Interval>2</t:Interval>
        </t:DailyRecurrence>
        <t:EndDateRecurrence>
            <t:StartDate>2013-12-24Z</t:StartDate>
            <t:EndDate>2014-02-21Z</t:EndDate>
        </t:EndDateRecurrence>""",
        "FREQ=DAILY;INTERVAL=2;UNTIL=20140222T000000Z"),
    ("""\
        <t:RelativeMonthlyRecurrence>
            <t:Interval>1</t:Interval>
            <t:DaysOfWeek>Wednesday</t:DaysOfWeek>
            <t:DayOfWeekIndex>Last</t:DayOfWeekIndex>
        </t:RelativeMonthlyRecurrence>
        <t:EndDateRecurrence>
            <t:StartDate>2014-01-29Z</t:StartDate>
            <t:EndDate>2014-12-17Z</t:EndDate>
        </t:EndDateRecurrence>""",
        "FREQ=MONTHLY;INTERVAL=1;BYDAY=-1WE;UNTIL=20141218T000000Z"),
]


head = """\
<?xml version="1.0" encoding="utf-8"?>
<dummyroot xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types">
    <t:Recurrence>
"""
tail = """\
    </t:Recurrence>
</dummyroot>
"""

for xml, result in CASES:
    fxml = head + xml + tail
    recNode = ET.fromstring(fxml)[0]
    real_result = exchangeRecurrence.RecurrenceEws2rfc2445().Convert(recNode)
    succeeded = result == real_result
    print "%-60s [%s]" % (real_result, "OK" if succeeded else "FAILED")