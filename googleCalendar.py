import datetime
import httplib2
import logging
import pprint

from apiclient.discovery import build
from apiclient.http import BatchHttpRequest
from oauth2client.file import Storage
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.tools import run

import rfc3339

import calendarEvent
import config

log = logging.getLogger(__name__)

GOOGLE_BATCH_LIMIT = 1000

class GoogleService:
    def __init__(self):
        # https://developers.google.com/google-apps/calendar/auth
        authflow = OAuth2WebServerFlow(
            client_id = config.GOOGLE_CLIENT_ID,
            client_secret = config.GOOGLE_CLIENT_SECRET,
            scope = 'https://www.googleapis.com/auth/calendar',
            redirect_url = 'urn:ietf:wg:oauth:2.0:oob')

        storage = Storage('calendar.dat')
        credentials = storage.get()
        if credentials is None or credentials.invalid == True:
            credentials = run(authflow, storage)

        self._http = httplib2.Http()
        self._http = credentials.authorize(self._http)

        self._service = build(serviceName='calendar', version='v3', http=self._http)
        self._InitBatch()

    def _InitBatch(self):
        self._batch = BatchHttpRequest() # batch is global for all calendars
        self._batch_counter = 0

    def GetCalendarList(self):
        return _GoogleCalendarList(self)

class _GoogleAPI:
    def __init__(self, gsvc):
        self._gsvc = gsvc
        self._instance_batch_counter = 0 # local counter to restrict Clear and Delete when >0

    def _DateCompose(self, dt):
        return rfc3339.datetimetostr(dt)

    def _DateParse(self, datestr):
        return rfc3339.parse_datetime(datestr)

    def Flush(self):
        log.debug("GoogleCalendar.Flush() - callbacks firing now")
        if self._gsvc._batch_counter > 0:
            self._gsvc._batch.execute(http=self._gsvc._http) # this returns None
            self._gsvc._InitBatch()
            self._instance_batch_counter = 0

    def _AddToBatch(self, req, reqBody=None, callback=None, callbackParams=None):
        # https://developers.google.com/api-client-library/python/guide/batch
        if callbackParams is None:
            callbackParams = {}
        if reqBody:
            callbackParams["request_body"] = reqBody
        def create_callback(params):
            if callback:
                return lambda a,b,c: callback(a, b, c, **params)
            else:
                return None
        if self._gsvc._batch_counter >= GOOGLE_BATCH_LIMIT:
            self.Flush()
        self._gsvc._batch.add(req, callback=create_callback(callbackParams))
        self._gsvc._batch_counter += 1
        self._instance_batch_counter += 1

    def _RequireEmptyBatch(self):
        if self._instance_batch_counter > 0:
            raise Exception("Unable to perform this operation with unempty batch")

class _GoogleCalendar(_GoogleAPI):
    def __init__(self, gsvc, calendarId, name, timezone):
        _GoogleAPI.__init__(self, gsvc)
        self._name = name
        self._timezone = timezone
        self._cid = calendarId

    def __str__(self):
        return "<GoogleCalendar name=%(_name)r timezone=%(_timezone)r id=%(_cid)r>" % self.__dict__

    def _GetEvents(self, startDate=None):
        if startDate:
            startDate = self._DateCompose(startDate)
        page_token = None
        events = []
        while True:
            resp = self._gsvc._service.events().list(
                calendarId=self._cid,
                timeMin=startDate,
                pageToken=page_token,
                maxResults=5000).execute()
            events += resp["items"]
            page_token = resp.get('nextPageToken')
            if not page_token:
                break
        return events

    def GetTimezone(self):
        return self._timezone

    def GetId(self):
        return self._cid

    def GetEvents(self, startDate=None):
        events = self._GetEvents(startDate)
        result = []
        for e in events:
            event = calendarEvent.CalendarEvent(
                eid         = e['id'],
                summary     = e['summary'],
                description = e.get('description'),
                start       = self._DateParse(e['start']['dateTime']),
                end         = self._DateParse(e['end']['dateTime']),
                location    = e.get('location'),
                timestamp   = self._DateParse(e['updated'])
            )
            result.append(event)
        return result

    def _ConvEvent(self, event, gevent=None):
        if gevent is None:
            gevent = {}
        gevent["summary"] = event.summary
        descr = event.description
        if event.organizer:
            descr = "Organizer: %s\n%s" % (event.organizer, descr)
        gevent["location"] = event.location
        gevent["start"] = {"dateTime":self._DateCompose(event.start), "timeZone":self.GetTimezone()}
        gevent["end"]   = {"dateTime":self._DateCompose(event.end),   "timeZone":self.GetTimezone()}
        if event.rrule:
            exdates = ["EXDATE:"+d.strftime("%Y%m%dT%H%M%SZ") for d in event.exdates]
            gevent["recurrence"] = ["RRULE:"+event.rrule] + exdates
        # email is mandatory, so some magic for email
        if config.SYNC_ATTENDEES_TO_DESCRIPTION:
            descr += "\n\nAttendees:\n"
            descr += "; ".join(["%s <%s>" % (a.name, a.email) for a in event.attendees])
        else:
            gevent["attendees"] = [
                { "displayName" : a.name,
                  "email"       : a.GetEmail(create_dummy_if_empty=True)
                } for a in event.attendees]
        gevent["description"] = descr
        if event.remindBefore is not None:
            gevent["reminders"] = {
                "overrides" : [ { "method":"popup", "minutes":event.remindBefore } ],
                "useDefault" : False,
            }
        return gevent

    def AddEvent(self, event, callback=None, callbackParams=None):
        gevent = self._ConvEvent(event)
        operation = self._gsvc._service.events().insert(calendarId=self._cid, body=gevent)
        self._AddToBatch(operation, gevent, callback, callbackParams)

    def UpdateEvent(self, event, eventId, callback=None, callbackParams=None):
        gevent = self._ConvEvent(event)
        operation = self._gsvc._service.events().update(calendarId=self._cid, eventId=eventId, body=gevent)
        self._AddToBatch(operation, gevent, callback, callbackParams)

    def ModifyOccurrence(self, eventId, occEvent, callback=None, callbackParams=None):
        instances = self._gsvc._service.events().instances(
            calendarId = self._cid,
            eventId = eventId,
            originalStart = self._DateCompose(occEvent.originalStart)
        ).execute()['items']
        if len(instances) != 1:
            raise Exception("Expected one result from instances() call, got %s:\n%s" %
                (len(instances), pprint.pformat(instances)))
        instance = self._ConvEvent(occEvent, instances[0])
        operation = self._gsvc._service.events().update(calendarId=self._cid, eventId=instance["id"], body=instance)
        self._AddToBatch(operation, instance, callback, callbackParams)

    def DeleteEvent(self, eventId, callback=None, callbackParams=None):
        operation = self._gsvc._service.events().delete(calendarId=self._cid, eventId=eventId)
        self._AddToBatch(operation, callback=callback, callbackParams=callbackParams)

    def Clear(self):
        err_count = 0
        def callback(request_id, response, exception, **kwargs):
            if exception:
                err_count += 1
        self._RequireEmptyBatch()
        events = self._GetEvents()
        for event in events:
            self.DeleteEvent(event['id'])
        self.Flush()
        if err_count > 0:
            raise Exception("Calendar cleaning failed, %s of %s operations failed" %
                (err_count, len(events)))
        log.debug("Cleared '%s' calendar (%d items)" % (self._name, len(events)))

    def Delete(self):
        self._RequireEmptyBatch()
        self._gsvc._service.calendars().delete(calendarId=self._cid).execute()

class _GoogleCalendarList(_GoogleAPI):
    def __init__(self, gsvc):
        _GoogleAPI.__init__(self, gsvc)
        self._LoadCalendars()

    def __iter__(self):
        return iter(self._calendars)

    def __len__(self):
        return len(self._calendars)

    def _LoadCalendars(self):
        raw_calendars = self._gsvc._service.calendarList().list().execute()['items']
        self._calendars = []
        self._by_name = {}
        self._by_id = {}
        self._primary = None
        for cal in raw_calendars:
            gcal = _GoogleCalendar(self._gsvc, cal['id'], cal['summary'], cal.get('timeZone'))
            self._calendars.append(gcal)
            self._by_name[cal['summary']] = gcal
            self._by_id  [cal['id']]      = gcal
            if cal.get("primary"):
                self._primary = gcal

    def GetCalendars(self):
        return self._calendars

    def GetPrimaryCalendar(self):
        return self._primary

    def GetCalendar(self, name=None, id=None):
        if (name is None) == (id is None):
            raise Exception("name or id must be specified")
        if name:
            return self._by_name.get(name)
        if id:
            return self._by_id.get(id)

    def GetDefaultTimezone(self):
        return self.GetPrimaryCalendar().GetTimezone()

    def Exists(self, calendar_name):
        return calendar_name in self._by_name

    def CreateCalendar(self, name, timezone=None):
        if not timezone:
            timezone = self.GetDefaultTimezone()
        data = {
            'summary': name,
            'timeZone': timezone
        }
        req = self._gsvc._service.calendars().insert(body=data)
        cid = req.execute()['id']
        return _GoogleCalendar(self._gsvc, cid, name, timezone)