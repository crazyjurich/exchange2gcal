import datetime
import logging

############# Exchange configuration:
EXCHANGE_USER = "ORGANIZATION\\jack.daniels"
EXCHANGE_PASSWORD = "password1"
EXCHANGE_URL = "https://WINSRV.organization.dummy/EWS/Exchange.asmx"


############# Google configuration:
# There should be no calendar with this name, calendar will be created on first synchronization
GOOGLE_CALENDAR_NAME = "Worksync"
# Following two values can be acquired on "Google Developers Console" -> "APIs & auth" -> "Credentials" -> "Create new client id"
GOOGLE_CLIENT_ID = ""
GOOGLE_CLIENT_SECRET = ""
# Note: google account and password will be asked on first synchronization


############# Synchronization configuration:
# You may want to skip syncing old events
# This parameter defines sync starting date
# Accepted values: None or date in the past
SYNC_PAST_LIMIT = datetime.date.today() - datetime.timedelta(days=10)
# Note that this parameter is ignored for recurring events

# You may disable syncing reminders
# When disabled you can configure default calendar reminders for all events on google.com/calendar
SYNC_REMINDERS = True

# Google has a limit of few hundred attendees ( for what period? hour? day? )
# When this happens you see "Calendar usage limits exceeded." errors
# This is workaround to append attendees to event description instead of creating real attendees
SYNC_ATTENDEES_TO_DESCRIPTION = False


############# Debug/development configuration:
# When enabled, all messages sent and received from Exchange server are stored in 'debug' directory
DEBUG_DUMP_MESSAGES = True
LOGLEVEL = logging.DEBUG